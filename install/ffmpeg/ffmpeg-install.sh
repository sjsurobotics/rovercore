sudo apt-get update
sudo apt-get -y --force-yes install autoconf automake build-essential libass-dev libfreetype6-dev \
  libtheora-dev libtool libvorbis-dev pkg-config texi2html zlib1g-dev

sudo apt-get -y --force-yes install yasm
sudo apt-get -y --force-yes install libx264-dev
sudo apt-get -y --force-yes install libmp3lame-dev
sudo apt-get -y --force-yes install libopus-dev

git clone git://source.ffmpeg.org/ffmpeg.git
cd ffmpeg
./configure --enable-gpl \
  --enable-libfreetype \
  --enable-libmp3lame \
  --enable-libopus \
  --enable-libx264 \
  --enable-libaacplus \
  --enable-nonfree

make
make install


Hey Charles,

Yes, by regulating the voltage from the 5V rail to the motors and the 5V rail you are separating the nodes and any massive current draw will be hit by the filtering caps on the regulator and the 9V supply. As long as the 9V supply does not dip too low, you should be fine. Another thing to worry about with motors making noise is the proximity to other signal wires and electronics. Also, Bindal did tell me that the noise can also make the ground noisy which is usually a problem with bigger motors and high rpm so you should be fine. 

2x7805 with 9V input should be fine.

I do not know either. Usually, if the signal is extremely noisy, I select a moderately sized ferrite bead (the ones you find on a USB A to B micro) and check to see if the noise is still there. The larger the bead, the more it can suppress the high frequency AC. I do not think it will effect any data lines you send information through. I would rather you use an RLC circuit for the power though. Ferrite beads can be used for power, but that is the last stage after the RLC parts.

There needs to be a central place where all of the grounds meet each other. For the rover, we had a block of metal that connected all of the 


----
Khalil Estell
http://kammce.io/
President of The S.oftware and C.omputer E.ngineering Society
http://sce.engr.sjsu.edu/
President of The SJSU Robotics Club
http://sjsurobotics.org/


On Fri, Jul 10, 2015 at 3:37 PM, Charles MacDonald <chamacd@gmail.com> wrote:
Hey!

Since you are a certified expert in motors, I had some questions about handling noise caused by motors. I was thinking about a circuit to control a floppy drive which contains a DC motor and a stepper motor, both powered from the same +5V source. My concern is that the motors will make that +5V rail very noisy and that will cause problems with other digital circuits.

From what I've read online, it's a good idea to make sure your digital circuits and motors have their own power source. It seems like using two different regulators would do the trick, like one 7805 for the digital and another 7805 for the motors. Both would be fed from the same +9V DC input. Does that sound about right?

Have you used ferrite beads to block high frequency noise? I'm trying to figure out how you select those and when they are appropriate to use. I see them referenced a lot in terms of reducing noise from motors, but nothing too concise about how you pick one. They seem to be highly resistive at certain frequency ranges, but I'd imagine for a power supply that's DC you'd want to pick one at a very low frequency since there should be no AC components beyond the ripple voltage that's like a few millivolts. Or maybe there's some kind of RC/LC/RLC filtering that's better for this?

Another big thing seems to be ensuring that your digital ground and your analog/motor ground only connect in exactly one place in the circuit. Don't suppose you know why that is?

Analog stuff is really confusing!

Regards,

Charles



On Thu, Jul 9, 2015 at 2:50 PM, Khalil Estell <khalil.estell@sjsu.edu> wrote:
Damn isolation!

That is epic man. You have to show me this thing working in real life.

Thanks for all of the help Charles!!

----
Khalil Estell
http://kammce.io/
President of The S.oftware and C.omputer E.ngineering Society
http://sce.engr.sjsu.edu/
President of The SJSU Robotics Club
http://sjsurobotics.org/


On Thu, Jul 9, 2015 at 2:32 PM, Charles MacDonald <chamacd@gmail.com> wrote:
Hey,

Yeah I glanced over it and I think the isolation part was the problem. So this other chip should be good. I guess isolation is a hard problem to solve (or at least it is for Texas Instruments).

Yep I made that, and there's more boards in the pipeline! I should have taken a picture of the bottom because all the traces are on that side, the top is a little boring. Basically it's a CPLD programmed to work like an I/O chip with 48 bidirectional pins. There's some cool stuff in there to speed up data throughput too since I had to read some large (4 megabyte) EPROMs with it recently.

Regards,

Charles

On Thu, Jul 9, 2015 at 2:25 PM, Khalil Estell <khalil.estell@sjsu.edu> wrote:
Holy shit?! Did you make that yourself?

The chip is TXB0108. And I think you are right.

----
Khalil Estell
http://kammce.io/
President of The S.oftware and C.omputer E.ngineering Society
http://sce.engr.sjsu.edu/
President of The SJSU Robotics Club
http://sjsurobotics.org/


On Thu, Jul 9, 2015 at 2:20 PM, Charles MacDonald <chamacd@gmail.com> wrote:
Oops yeah, what was that bad TI part you used? Then I can check if this part uses the same technology or not. I think that bad part had galvanic isolation or something which was the weird aspect of the chip, which this one does not. 

I haven't used the TCA9406 myself. So it might be worthwhile to just get one, breadboard something up, and see how it performs.
If you are doing a shitton of I2C channels then I can see why that 22 bit part sounds good. I think that's a case where I'd also want to get a tssop breakout board and one GTL2000, and try testing it too.

When it comes to voltage translation you can never be too cautious. :D

Btw here's the latest thing I got from bay area circuits:

https://www.dropbox.com/s/mahig8seox3k6s9/reader.png?dl=0

Universal EPROM reader! You just hook up jumper wires for 5V and ground to the appropriate pins and it does the rest automatically.

Regards,

Charles


On Thu, Jul 9, 2015 at 2:05 PM, Khalil Estell <khalil.estell@sjsu.edu> wrote:
Also, I can solder TSSOP, easy!

----
Khalil Estell
http://kammce.io/
President of The S.oftware and C.omputer E.ngineering Society
http://sce.engr.sjsu.edu/
President of The SJSU Robotics Club
http://sjsurobotics.org/


On Thu, Jul 9, 2015 at 2:05 PM, Khalil Estell <khalil.estell@sjsu.edu> wrote:
Thank Charles for the advice!!

I looked at the other chip and it looks great! My only fear is using a TI I2C part. Remember what happened last time with the other TI bidirection chip. So... much noise.... so much high frequency noise!

Have you ever used the TCA9406 chip before?

The point of using the 22 bit version (there are 2bit all the way up to 22 bit versions) is to pretty much translate every single GPIO on the ODROID. Not super necessary, but may come in handy. Also, I like the idea of using military grade stuff. If we had a full ITAR miltary grade rover, that would be awesome!!! It would need to have lasers and a rocket launch as well.

----
Khalil Estell
http://kammce.io/
President of The S.oftware and C.omputer E.ngineering Society
http://sce.engr.sjsu.edu/
President of The SJSU Robotics Club
http://sjsurobotics.org/


On Thu, Jul 9, 2015 at 1:05 PM, Charles MacDonald <chamacd@gmail.com> wrote:
You know I've only ever heard of GTL in military applications, and I've never seen GTL parts used outside of that context.

Assuming you are using this for I2C, the datasheet has a pretty specific case of how that's done using open collector signals, so you'll need a few pull-up resistors. This means whatever is driving the signals needs to have open collector outputs or you'd need to add hardware to make that happen. (page 4 of http://www.nxp.com/documents/data_sheet/GTL2000.pdf)

Do you really need 22 channels? (I know the rover has 178,494 I2C peripherals, so you may! :) This is just 5V to 3.3V bidirectional on SCL and SDA?

Personally I think a few of these might be better:

http://www.ti.com/lit/ds/symlink/tca9406.pdf

The DCU package is SOIC it looks like, so you can hand solder it. It does 1.65V-3.3V to 5.0V translation. and you don't need any open collector stuff.

What do you think?

Regards,

Charles



On Thu, Jul 9, 2015 at 12:38 PM, Khalil Estell <khalil.estell@sjsu.edu> wrote:
Whoops!!
http://www.nxp.com/products/interface_and_connectivity/gtl_to_ttl_translators/series/GTL2000.html#ordering
On Jul 9, 2015 12:15 PM, "Charles MacDonald" <chamacd@gmail.com> wrote:
Hey,

Was there a link or datasheet you were going to include?

Regards,

Charles

On Thu, Jul 9, 2015 at 8:32 AM, Khalil Estell <khalil.estell@sjsu.edu> wrote:
How do you feel about this board to do voltage translation from 1.8V to 3.3V or 5V. Is it too good to be true? I am probably going to get some samples, but I still want to know what you think.

If the translation works, then I will probably be going with the odroid to do everything, including video processing.

On Jun 28, 2015 10:23 AM, "Charles MacDonald" <chamacd@gmail.com> wrote:
My phone is on now!!!! My bad have been sick and sleeping a lot.

On Sun, Jun 28, 2015 at 10:11 AM, Khalil Estell <khalil.estell@sjsu.edu> wrote:
Charles??

----
Khalil Estell
http://kammce.io/
President of The S.oftware and C.omputer E.ngineering Society
http://sce.engr.sjsu.edu/
President of The SJSU Robotics Club
http://sjsurobotics.org/


On Sat, Jun 27, 2015 at 4:22 PM, Khalil Estell <khalil.estell@sjsu.edu> wrote:
I have more questions Charles. Tell me when you are free.

----
Khalil Estell
http://kammce.io/
President of The S.oftware and C.omputer E.ngineering Society
http://sce.engr.sjsu.edu/
President of The SJSU Robotics Club
http://sjsurobotics.org/


On Wed, Jun 3, 2015 at 5:53 PM, Khalil Estell <khalil.estell@sjsu.edu> wrote:
Damn dude! That is higher than my sex hotline costs per minute. But ok, calling....






----
Khalil Estell
http://kammce.io/
President of The S.oftware and C.omputer E.ngineering Society
http://sce.engr.sjsu.edu/
President of The SJSU Robotics Club
http://sjsurobotics.org/


On Wed, Jun 3, 2015 at 5:51 PM, Charles MacDonald <chamacd@gmail.com> wrote:
My phone is on, charged, and ready for dialing. Just $5.00/minute for tech advice. My number is 510 305-2194.

Regards,

Charles

On Wed, Jun 3, 2015 at 5:48 PM, Khalil Estell <khalil.estell@sjsu.edu> wrote:
Since you are no longer around and I can no longer bother you whilst working to talk to you about stuff, can I call you to ask you a question about i2c.

----
Khalil Estell
http://kammce.io/
President of The S.oftware and C.omputer E.ngineering Society
http://sce.engr.sjsu.edu/
President of The SJSU Robotics Club
http://sjsurobotics.org/















