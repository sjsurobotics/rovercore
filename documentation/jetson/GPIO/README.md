GPIO
====

http://elinux.org/Jetson/GPIO

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |

GPIO_PU0	gpio160	Pin 40 on J3A2	
GPIO_PU1	gpio161	Pin 43 on J3A2	
GPIO_PU2	gpio162	Pin 46 on J3A2
GPIO_PU3	gpio163	Pin 49 on J3A2	
GPIO_PU4	gpio164	Pin 52 on J3A2	
GPIO_PU5	gpio165	Pin 55 on J3A2	
GPIO_PU6	gpio166	Pin 58 on J3A2	

General Purpose Outputs
GPIO_PH1	gpio57 Pin 50 on J3A1	
GPIO_PK1	gpio81 Pin 48 on J3A1
GPIO_PK2	gpio82 Pin 17 on J2A1
GPIO_PK4	gpio84 Pin 13 on J3A1

General Purpose Inputs
GPIO_PCC1	gpio225 Pin 57 on J3A2
GPIO_PCC2	gpio226 Pin 7 on J3A2 
